

import fs from 'fs-extra';
import {execSync as exec} from 'child_process';
import path from 'path';


const SRC = path.resolve('./src');
const DIST = path.resolve('./dist');


const SRC_SHARED = path.join(SRC, 'shared');
const SRC_SERVER = path.join(SRC, 'server');
const DIST_SHARED = path.join(DIST, 'shared');
const DIST_SERVER = path.join(DIST, 'server');

fs.removeSync(DIST);
fs.mkdirsSync(DIST);
fs.copySync(`${SRC_SHARED}`, `${DIST_SHARED}`);
fs.copySync(`${SRC_SERVER}`, `${DIST_SERVER}`);

exec(`babel ${SRC_SHARED} -d ${DIST_SHARED}`, { stdio: 'inherit' });
exec(`babel ${SRC_SERVER} -d ${DIST_SERVER}`, { stdio: 'inherit' });
exec( `webpack --progress --color  --display-chunks`, { stdio: 'inherit' } );






