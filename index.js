

require('babel-register');
require('dotenv').load();

var path  = require('path');

var os = require('os');

var networkInterfaces = os.networkInterfaces();

var nif = networkInterfaces['en0'] || networkInterfaces['en1'] || networkInterfaces['WiFi'];

if (!nif) {
    throw new Error('cannot find network interfaces');
}

var address = nif.filter(function (val) {
    return val.family === 'IPv4'
})[0].address;


var serverPath = path.resolve(
    __dirname,
    process.env.NODE_ENV === 'production' ? 'dist/server/' : 'src/server'
);


var server = require(serverPath).default({address: address, port: 3001});