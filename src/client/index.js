

import './reset.scss'
import './style.scss';
import UI from './ui';
import Message from '../shared/message';
import Recognition from './speech-recognition';
import {BotNameList as Names} from '../shared/config';
import createPauseScreen from './pauseScreen';
import socket from './socket';
import $ from 'jquery';



if(!localStorage.getItem('messages')){
    localStorage.setItem('messages', JSON.stringify([]));
}

function loadMessages(){
    return JSON.parse(localStorage.getItem('messages'));
}

function saveMessage(message){
    let messages = loadMessages();
    if(messages.length > 50){
        messages.shift();
    }
    messages.push(message);
    localStorage.setItem('messages', JSON.stringify(messages));
}




let $pauseContainer = $('<div>').appendTo('body');
createPauseScreen({$container: $pauseContainer});
$pauseContainer.hide();

loadMessages().forEach(msg => {
    UI.displayMessage(msg);
});

socket.on('connect', function(){

    console.log('socket connected');
    let rek = Recognition();
    init(socket, rek);

    socket.on('message', message => {
        UI.displayMessage(message);
        saveMessage(message);
    });


    socket.on('pause', () => {
        $pauseContainer.show();
        rek.pause();
    });
    socket.on('resume', () => {
        $pauseContainer.hide();
        rek.resume();
    });

    socket.on('refreshRek', () => {
        console.log('refreshing rek');
        location.reload();
        // rek = Recognition();
        // init(socket, rek);

    });

});

let canReload = true;

const init = (socket, rek) => {
    rek.init();
    rek.onProgress(text => {
        canReload = false;
        // console.log('progress', text);
        UI.header.render(Recognition.States.PROGRESS, text);
    });

    rek.onResult(text => {
        console.log("result", text);
        UI.header.render(Recognition.States.RESULT, text);

        let recipient = Names[Math.floor(Math.random()*Names.length)];
        let message = Message('Human', text, recipient, null, true);

        UI.displayMessage(message);
        socket.emit('message', message);
        setTimeout(() => {
            UI.header.render(Recognition.States.STANDBY);

            setTimeout(() => {
                canReload = true;
            }, 10000);


        }, 10000);
    });

// testMessages.forEach(UI.displayMessage);

    UI.header.render(Recognition.States.STANDBY);

};

setInterval(() => {
    if(canReload){
        location.reload();
    }
}, 20000);




