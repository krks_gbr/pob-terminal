
import Message from '../../../../../../shared/message';
import Config from '../../../../config';



//TODO MAKE THESE
// #input
//      form
//          input#clientName(type="text" placeholder="Your Name")
//          input#clientMessage(type="text" placeholder="Your Message")
//          input#submitButton(type="submit")

let $form = $('<form>').appendTo('#input');


$form.submit(e => {
    e.preventDefault();
    let sender = $('#clientName').val();
    let content = $('#clientMessage').val();  $('#clientMessage').val('');
    $('#clientMessage').attr('placeholder', '');
    let botNames = Config.BotNames;
    let message = Message(
        sender,
        content,
        botNames[Math.floor(Math.random()*botNames.length)],
        true
    );

    // socket.emit('message', message);
});