

import $ from 'jquery';
import {States} from '../../speech-recognition';



const Header =  () => {

    let $header = $('<header>').attr('id', 'header').addClass('row');
    let logoSVGString = require('../../logo.svg');

    $('<div>').html(logoSVGString).attr('id', 'logo').appendTo($header);
    let $status = $('<div>').attr('id', 'status').appendTo($header);


    function renderText(text, final=false){
        $status.empty();
        let $container = $('<div>').attr('id', 'userMessageContainer').appendTo($status);
        let $userName = $('<div>').addClass('userName').text("you: ").appendTo($container);
        let $userMessage = $('<div>').addClass('userMessage').text(text).appendTo($container);
        if(final){
            $container.addClass('highlight');
            setTimeout(() => $container.removeClass('highlight'), 2000);
        }
    }
    
    return {
        
        $: $header,
        render: (state, text) => {
            // console.log('rendering ', state);
            switch (state){

                case States.STANDBY:
                    $status.empty();
                    let $textContainer = $('<div>').attr('id', 'standbyText').appendTo($status);
                    $textContainer.append($('<div>').text('Your opinion is important to us.'));
                    $textContainer.append($('<div>').text('Let your voice be heard.'));
                    break;

                case States.PROGRESS:
                    renderText(text);
                    break;

                case States.RESULT:
                    renderText(text, true);
                    break;

            }

        },
        
        
    }

};

export default Header;