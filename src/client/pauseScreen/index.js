

import $ from 'jquery';
import './style.scss';
import logoSVGString from '../logo.svg';

export default ({$container}) => {

    $container.addClass('pauseContainer');
    let $pauseScreen = $('<div>').attr('class','pauseScreen').appendTo($container);
    let svg = $(logoSVGString).appendTo($pauseScreen);

}