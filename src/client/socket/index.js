
import io from 'socket.io-client';

export default io(`${process.env.POB_HOST}${window.__nsp__}`, {forceNew: true});