


/**
 * Factory that creates a Message object.
 * @function Message 
 * @param {string} sender
 * @param {string} content
 * @param {string || null} recipient
 * @param {string || null} unitID
 * @param {boolean} [fromHuman=false] - if the message comes from a human
 * @returns {Message}
 * @constructor
 */

const Message =  (sender, content, recipient, unitID=null, fromHuman=false) =>
    (
        /**
         * @typedef  {Object} Message
         * @property {string} sender
         * @property {string} content
         * @property {string || null} recipient
         * @property {boolean} fromHuman
         * @property {string} unitID
         */
        {
            sender: sender,
            content: content,
            recipient: recipient,
            fromHuman: fromHuman,
            unitID: unitID,
        }
    );

export default Message;




