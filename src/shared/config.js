



let Names = {
    TEOL: "Teol",
    VOLLH: "Vollh",
    GIBS: "Gibs",
    AUMNA: "Aumna",
};

let TTS_VOICES =  {
    LEE: "Lee",
    SAMANTHA: "Samantha",
    DANIEL: "Daniel",
    TOM: "Tom",
    TESSA: "Tessa"
};

let VoiceMapping = {

    [Names.TEOL]: TTS_VOICES.SAMANTHA,
    [Names.VOLLH]: TTS_VOICES.LEE,
    [Names.GIBS]: TTS_VOICES.DANIEL,
    [Names.AUMNA]: TTS_VOICES.TESSA

};

export {VoiceMapping};

let NameList = Object.keys(Names).map(key => Names[key]);


export {Names as BotNames};
export {NameList as BotNameList};
