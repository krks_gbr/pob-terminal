import path from "path";
import {BotNames, BotNameList} from '../shared/config';





let ROOT_PATH =  path.dirname(require.main.filename);

let ENV_PATH = path.resolve(
    ROOT_PATH,
    process.env.NODE_ENV === 'production' ? 'dist' : 'src',
);

let Config = {
    BotNameList: BotNameList,
    Bots: {
        ...BotNames,
    },
    STORAGE_DIR:    path.join(ROOT_PATH, "storage"),
    TEMPLATES_DIR:  path.join(ENV_PATH, "server", "templates"),
    PUBLIC_DIR:     path.join(ROOT_PATH, "dist", "client")
};


export default Config;