
import http from 'http';
import Config from './config';
import Express from 'express';


export default ({address, port}) => {

    var app = Express();
    var server = http.createServer(app);

    app.set('views', Config.TEMPLATES_DIR);
    app.set('view engine', 'pug');

    app.use( Express.static( Config.PUBLIC_DIR ));

    app.locals.host = `http://${address}:${port}`;

    app.get('/', function(req, res){
        res.locals.nsp = '/terminal';
        res.render('terminal');
    });


    server.listen(port, function(){
        console.log('terminal running on ' + address + ":" + port);
    });
    
    return server;
};



