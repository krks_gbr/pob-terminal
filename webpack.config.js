
require('dotenv').load();
var path = require('path');
var ExtractTextPlugin = require("extract-text-webpack-plugin");

var extractCSS = new ExtractTextPlugin(path.join("css", '[name].css'));
var webpack = require('webpack');

var src = path.join(__dirname, 'src');


var processEnv = {
    'process': {
        'env': {
            'NODE_ENV': `'${process.env.NODE_ENV}'`,
            'POB_HOST': `'${process.env.POB_HOST}'`
        },
    }
};


module.exports = {
    entry: path.join(__dirname, "src", "client"),
    output: {
        path:     path.join(__dirname, "dist", "client"),
        filename: '[name].bundle.js'
    },
    module:{
        loaders:[

            { test: /\.(png|woff|woff2|eot|ttf)$/, loader: 'url-loader?limit=100000' },

            {
                test: /\.svg$/,
                loader: 'svg-inline'
            },
            {
                test:   /\.js/,
                loader: 'babel',
                include: [
                    path.join(src, "client"),
                    path.join(src, "shared")
                ],
            },
            {
                test: /\.scss$/,
                loader: extractCSS.extract("style-loader", `css!sass`)
            },
            {
                test:   /\.html/,
                loader: 'html',
            }
        ]
    },

    node: {
        fs: "empty"
    },
    
    plugins:[
        extractCSS,
        new webpack.DefinePlugin(processEnv)

    ]

};